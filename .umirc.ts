import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    // 配置微应用 app1 关联的路由
    {
      path: '/app1/project',
      microApp: 'app1',
    },
    { path: '/', component: '@/pages/index' },
  ],
  fastRefresh: {},
  qiankun: {
    master: {
      // 注册子应用信息
      apps: [
        {
          name: 'app1', // 唯一 id
          entry: '//localhost:8001', // html entry
        },
        // {
        //   name: 'app2', // 唯一 id
        //   entry: '//localhost:8000', // html entry
        // },
      ],
    },
  },
});
